/*[attr] – по имени атрибута;
[attr=value] – по имени и значению атрибута;
[attr^=value] – по имени и значению, с которого оно должно начинаться;
[attr|=value] – по имени атрибута и его значению, которое равно value или начинается со value-;
[attr$=value] – по имени атрибута и значению, на которое оно должно заканчиваться;
[attr*=value] – по указанному атрибуту и значению, которое должно содержать value;
[attr~=value] – по имени атрибута и значению, которое содержит value отделённое от других с помощью пробела.*/

let _img = document.querySelector('img[src^="./src/easter-happy.gif"]');
if (_img != null) {
  _img.setAttribute("width", 256);
  _img.setAttribute("height", 256);
  _img.onclick = function () {
    let _link = _img.getAttribute("src");
    if (_link === "./src/easter-happy.gif")
      _img.setAttribute("src", "./src/pikachu.gif");
    else _img.setAttribute("src", "./src/easter-happy.gif");
  };
}

var myButton = document.querySelector("button");
var myHeading = document.querySelector("h1");

function setUserName() {
  var myName = prompt("Please enter your name.");
  localStorage.setItem("name", myName);
  myHeading.innerHTML = "This user is, " + myName;
}

if (localStorage.getItem("name") == null) {
  setUserName();
} else {
  var storedName = localStorage.getItem("name");
  myHeading.innerHTML = "This user is, " + storedName;
}

myButton.onclick = () => setUserName();
