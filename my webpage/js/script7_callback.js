//Колбэк-функция (от английского callback function) — это обычная функция, которая вызывается внутри другой функции.
// колбэк-функция
function cb() {
  console.log("this is callback");
}
// функция, которая будет принимать на вход колбэк-функцию
function fnWithCb(cbFn) {
  console.log("before calling the callback function");
  cbFn();
}
// вызываем функцию fnWithCb() и передаём ей в качестве аргумента колбэк-функцию cb
fnWithCb(cb);

console.log("------------------------------------");

// объявим колбэк-функцию
function printToLog(message) {
  console.log("переданный оргумент -", message);
}
// объявим функцию, имеющую 3 параметра
function sum(num1, num2, callback) {
  // вычислим сумму 2 значений и запишем его в result
  const result = num1 + num2;
  // вызовем колбэк-функцию
  callback(result);
}
// вызовем функцию sum
sum(5, 11, printToLog);

console.log("------------------------------------");

/*
// колбэк-функция
function setColorBody() {
  document.body.style.backgroundColor = "#00ff00";
}
// функция, которая будет вызывать функцию setColorBody через 3 секунды
setTimeout(setColorBody, 3000);
*/

function sum2(a, b) {
  return Number(a + b);
}

console.dir(sum2);
console.log(sum2.name); // sum2
console.log(sum2.length); //2 orgements

//Узнать, является ли переменная функцией, можно с помощью typeof:

function myFunc() {}

console.log(typeof myFunc); // function

console.log(
  "Например, проверим является переменная колбэк функцией перед тем её вызвать:"
);

function sum(num1, num2, callback) {
  const result = num1 + num2;
  if (typeof callback === "function") {
    callback(result);
  }
}

sum(1, 1, printToLog); //переданный оргумент - 2

//////////////////////////стрелочные функции//////////////////////
// let f1 = (arg1,arg2, ...argN) = expression;
//или
// let f1 = function(arg1,arg2, ...argN){
// return expression
// }

let sum3 = (a, b) => a + b;
/* Эта стрелочная функция представляет собой более короткую форму:

let sum3 = function(a, b) {
  return a + b;
};
*/
console.log(sum3(1,10));

//Если у нас только один аргумент, то круглые скобки вокруг параметров можно опустить, сделав запись ещё короче:

let double = n => n*2;
console.log (double(3));


//Если аргументов нет, круглые скобки будут пустыми, но они должны присутствовать:

let sayHi = () => alert("hello");
sayHi();

/*Стрелочные функции можно использовать так же, как и Function Expression.

Например, для динамического создания функции:
*/
let age = prompt("Сколько Вам лет?", 18);

let welcome = (age < 18) ?
  () => alert('Привет!') : () => alert("Здравствуйте!");

welcome();


//Многострочные стрелочные функции

let sum4 = (a, b) => {  // фигурная скобка, открывающая тело многострочной функции
  let result = a + b;
  return result; // если мы используем фигурные скобки, то нам нужно явно указать "return"
 };
 
 alert( sum4(1, 2) ); // 3