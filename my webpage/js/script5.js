"use strict"; 

/**Методы
getElementById — поиск элемента по идентификатору;
getElementsByClassName — поиск элементов по названию класса;
getElementsByTagName — поиск элементов по названию тега;
querySelector — поиск первого элемента, подходящего под CSS-селектор;
querySelectorAll — поиск всех элементов, подходящих под CSS-селектор. 
*/ 


/*пример работы с аттрибутами элемента ноды */
var table = document.getElementById("table");
var tableAttrs = table.attributes; // Node/Element interface
for (var i = 0; i < tableAttrs.length; i++) {
    console.log('ok0');
    console.log(tableAttrs.item(i));
   // HTMLTableElement interface: border attribute
   // if(tableAttrs[i].nodeName.toLowerCase() == "border")
   if(tableAttrs[i].nodeName.toLowerCase() == "style")
   {
    console.log('ok-border');
    table.style = "border: 3px solid yellowgreen";    
   }
} 
tableAttrs.getNamedItem('style').nodeValue = "border: 12px solid red"
// HTMLTableElement interface: summary attribute
table.summary = "note: increased border";