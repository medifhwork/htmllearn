/*
Массивы: перебирающие методы

*/

//"use strict";
// объявим массив однотипных анонимных объектов подопытных людей
const people = [{ name : 'Ann', age: 21, salary : 40000},
                { name : 'Mix', age: 23, salary : 45666},
                { name : 'Den', age: 28, salary : 45000},
                { name : 'Stas', age: 25, salary : 43000},
                { name : 'Zuhra', age: 23, salary : 30000},
                { name : 'Stas', age: 21, salary : 44000}
               ];

console.log("по классике обычный for");
for (let i= 0; i < people.length; i++) {
    console.log(people[i]);
} 

console.log("сокрашенный инициатор for -  let oneitem of array");
for (let e of people){
    console.log(e);
}    

/*
forEach
Метод arr.forEach(callback[, thisArg]) используется для перебора массива. Он для каждого элемента массива вызывает функцию callback. Этой функции он передаёт три параметра callback(item, i, arr):

item — очередной элемент массива;
i — его номер;
arr — массив, который перебирается.*/
console.log("foreach с тремя оргументами employe,index,pArr");
people.forEach( function(employe,index,pArr){
console.log (employe); //item
console.log (index); // итый эдемент
console.log (pArr); // сам массив
} );   // можно обратиться к соседним элементам 

// people.forEach(function(e){console.log(e)});

//people.forEach(e => console.log(e));

//const foo = people.map(e => {return e.age+10});



//MAP
/*  возврашает новый массив по количеству элементов оригинального массива,
    но польность сформированный переданной Функцией */
/*Метод arr.map(callback[, thisArg]) используется для трансформации массива.
Он создаёт новый массив, который будет состоять из результатов вызова callback(item, i, arr) для каждого элемента arr.*/

// const foo = people.map(e =>  `${e.name+`__`}, (${e.age}) `);
// console.log(foo);
// foo.forEach(e => console.log(e));

//прокаченный МАP ...restElem хранит в себе все элементы кроме указанных до "..." через запятую, удобненько дале в формирующей фенкции определяем исключенные поля по новой.
//     => ({     -важно!!!! добавить круглые скобки, что бы функция воспринималась как объект а не ссылка 
//ограничнный фигурными скобками
//const myarr = people.map( ({name, ...restElem}, index) => ({...restElem, name: `${name}_${index}`}));
//console.log(myarr);


//Filter
//Он создаёт новый массив, в который войдут только те элементы arr, для которых вызов callback(item, i, arr) возвратит true.

/*oldway*/
//const arrNew = []; //ининцилизируем массив обязательно иначе методы будут не доступны.

// for (let i=0; i< people.length; i++)
// {
//    if ( people[i].age == 21 ) {arrNew.push(people[i])}
// }
// console.log(arrNew);

//modern way
// const newa = people.filter((e)=>{
//     if (e.age==21)
//     {
//         return true
//     }
// });
// console.log (newa);
// const newar = people.filter((e)=>e.age==21?true:false);
// console.log (newar);
// const newarr=people.filter( (e)=> e.age==21);
// console.log (newarr);

//REDUCE
//old
// let amaunt = 0 ;
// for (let i=0; i< people.length; i++)
// {
//    amaunt += people[i].salary
// }
// console.log(amaunt);
// //modern
// const amaunt2 = people.reduce(
//     (total, item) => { return total+item.salary},
//     0
//     );
// console.log(amaunt2);

//станно но файнды возврашают только первое вхождение пичаль но вот.
// const a = people.find((item) =>  item.name ==='Stas');
// console.log (a) ;
// const b = people.findIndex((item) =>  item.name ==='Stas');
// console.log (b) ;


/*EVERY/SOME
Эти методы используются для проверки массива.
Метод arr.every(callback[, thisArg]) возвращает true, если вызов callback вернёт true для каждого элемента arr.
Метод arr.some(callback[, thisArg]) возвращает true, если вызов callback вернёт true для какого-нибудь элемента arr.
*/

/*  пытаюсь отрисовать результат в таблицу
 const newPeopople = people
 .filter(person => person.age > 21)
 .map(({salary, ...restOter},index) =>( {indexMMVB: index, money: salary+100000,...restOter}) );


 const myarr = people.map( ({name, ...restElem}, index) => ({...restElem, name: `${name}_modifyname`,index}));
 console.log(myarr); 

    let t_ = document.createElement('table');            
   // t_.setAttribute("style", "border: 2px solid black");
    myarr.forEach(c => {
        let tr_ = document.createElement('tr');
      //  tr_.setAttribute("style","border: 1px solid black");       
        tr_.innerHTML =
        //oldschool
        // "<td>" + c.index  + "<td/>"+
        // "<td>" + c.name   + "<td/>"+
        // "<td>" + c.salary + "<td/>"
        
        // //new school
        `
        <td>${c.index}<td/>
        <td>${c.age}<td/>
        <td>${c.name} <td/>
        <td>${c.salary}<td/>
        ` 
        let tdLst = tr_.getElementsByTagName('td');
        // tdLst.forEach(function(item, index, array){ tdLst.item(index).setAttribute("style", "border: 2px solid black");  }  );
        for ( let i=0; i< tdLst.length;i++){
        tdLst.item(i).setAttribute("style", "border: 1px dotted yellow");
        
        for ( let i=0; i< tdLst.length;i++){
            if (tdLst.item(i).innerText ===""){
                tdLst.item(i).remove(); 
            }
        }
    

       t_.appendChild(tr_);     
      
       
      }
    });
    document.body.appendChild(t_);
*/
        

    
      
